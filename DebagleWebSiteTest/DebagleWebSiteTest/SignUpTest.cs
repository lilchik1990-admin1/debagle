using DebagleWebSiteTest.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace DebagleWebSiteTest
{
    public class Tests
    {
        IWebDriver driver;
        private const string successRegistrationURL = "https://debugle.com/thank-you";
        private const string errorRegistrationURL = "https://debugle.com/signup";
        [SetUp]
        public void Setup()
        {
            
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://debugle.com/");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [Theory]
        [TestCase("Olha","Ivanova","hfxnfxnh@gmail.com", "1234", "1234", successRegistrationURL)]
        public void CheckRegistrationWithValidData(string fname, string lname, string email, string password, string passwordconfirm,string expUrl)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignUpPajeObgect signUpPajeObgect = new SignUpPajeObgect(driver);
            var signupbutt = startPageObgect.GetSignUpButton();
            signupbutt.Click();
            Thread.Sleep(300);
            signUpPajeObgect.SignUp(fname, lname, email, password, passwordconfirm);
            var actualurl = signUpPajeObgect.GetUrl();
            Assert.AreEqual(actualurl, expUrl);
        }
        [Theory]
        [TestCase("Olha", "Ivanova", "hwdhqqgmail.com", "1234", "1234", errorRegistrationURL, "The email address is invalid. Please enter a valid email address.")]
        public void CheckRegistrationWithNotValidEmail(string fname, string lname, string email, string password, string passwordconfirm, string expUrl,string experrorr)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignUpPajeObgect signUpPajeObgect = new SignUpPajeObgect(driver);
            var signupbutt = startPageObgect.GetSignUpButton();
            signupbutt.Click();
            Thread.Sleep(300);
            signUpPajeObgect.SignUp(fname, lname, email, password, passwordconfirm);
            var actualurl = signUpPajeObgect.GetUrl();
            var actualerror = signUpPajeObgect.GetEmailError();
            Assert.AreEqual(actualurl, expUrl);
            Assert.AreEqual(actualerror, experrorr);

        }
        [Theory]
        [TestCase("Olha", "Ivanova", "hefwfhqq@gmail.com", "1234", "1234", errorRegistrationURL, "The password must have at least 4 chars")]
        public void CheckRegistrationWithNotValidPassword(string fname, string lname, string email, string password, string passwordconfirm, string expUrl, string experrorr)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignUpPajeObgect signUpPajeObgect = new SignUpPajeObgect(driver);
            var signupbutt = startPageObgect.GetSignUpButton();
            signupbutt.Click();
            Thread.Sleep(300);
            signUpPajeObgect.SignUp(fname, lname, email, password, passwordconfirm);
            var actualurl = signUpPajeObgect.GetUrl();
            var actualerror = signUpPajeObgect.GetPasswordError();
            Assert.AreEqual(actualurl, expUrl);
            Assert.AreEqual(actualerror, experrorr);

        }
        [Theory]
        [TestCase("Olha", "Ivanova", "hwddfwhqq@gmail.com", "1234", "1234", errorRegistrationURL, "The password confirmation does not match password")]
        public void CheckRegistrationWithNotValidPasswordConfirm(string fname, string lname, string email, string password, string passwordconfirm, string expUrl, string experrorr)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignUpPajeObgect signUpPajeObgect = new SignUpPajeObgect(driver);
            var signupbutt = startPageObgect.GetSignUpButton();
            signupbutt.Click();
            Thread.Sleep(300);
            signUpPajeObgect.SignUp(fname, lname, email, password, passwordconfirm);
            var actualurl = signUpPajeObgect.GetUrl();
            var actualerror = signUpPajeObgect.GetPasswordConfirmError();
            Assert.AreEqual(actualurl, expUrl);
            Assert.AreEqual(actualerror, experrorr);

        }
        [Theory]
        [TestCase("", "Ivanova", "hwdhqail@mail.com", "1234", "1234", errorRegistrationURL, "Please enter your first name")]
        public void CheckRegistrationWithNotValidFirstName(string fname, string lname, string email, string password, string passwordconfirm, string expUrl, string experrorr)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignUpPajeObgect signUpPajeObgect = new SignUpPajeObgect(driver);
            var signupbutt = startPageObgect.GetSignUpButton();
            signupbutt.Click();
            Thread.Sleep(300);
            signUpPajeObgect.SignUp(fname, lname, email, password, passwordconfirm);
            var actualurl = signUpPajeObgect.GetUrl();
            var actualerror = signUpPajeObgect.GetFirstNameError();
            Assert.AreEqual(actualurl, expUrl);
            Assert.AreEqual(actualerror, experrorr);

        }
        [Theory]
        [TestCase("Olha", "", "hwdhqqgmail@mail.com", "1234", "1234", errorRegistrationURL, "Please enter your last name")]
        public void CheckRegistrationWithNotValidLastName(string fname, string lname, string email, string password, string passwordconfirm, string expUrl, string experrorr)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignUpPajeObgect signUpPajeObgect = new SignUpPajeObgect(driver);
            var signupbutt = startPageObgect.GetSignUpButton();
            signupbutt.Click();
            Thread.Sleep(300);
            signUpPajeObgect.SignUp(fname, lname, email, password, passwordconfirm);
            var actualurl = signUpPajeObgect.GetUrl();
            var actualerror = signUpPajeObgect.GetLastNameError();
            Assert.AreEqual(actualurl, expUrl);
            Assert.AreEqual(actualerror, experrorr);

        }
        [Theory]
        [TestCase("", "Ivanova", "hwdhqq@gmail.com", "1234", "1234", errorRegistrationURL)]
        [TestCase("Olha", "", "hwdhqq@gmail.com", "1234", "1234", errorRegistrationURL)]
        [TestCase("Olha", "Ivanova", "", "1234", "1234", errorRegistrationURL)]
        [TestCase("Olha", "Ivanova", "hwdhqq@gmail.com", "", "", errorRegistrationURL)]
        [TestCase("", "", "hwdhqq@gmail.com", "", "", errorRegistrationURL)]
        [TestCase("", "", "", "", "", errorRegistrationURL)]

        public void CheckRegistrationWithEmptyFields(string fname, string lname, string email, string password, string passwordconfirm, string expUrl, string experrorr)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignUpPajeObgect signUpPajeObgect = new SignUpPajeObgect(driver);
            var signupbutt = startPageObgect.GetSignUpButton();
            signupbutt.Click();
            Thread.Sleep(300);
            signUpPajeObgect.SignUp(fname, lname, email, password, passwordconfirm);
            var actualurl = signUpPajeObgect.GetUrl();
            Assert.AreEqual(actualurl, expUrl);

        }

    }
}