﻿using DebagleWebSiteTest.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;


namespace DebagleWebSiteTest
{
    class ProjectTest
    {
        IWebDriver driver;
        private const string UrlAfterRemoveproject = "https://debugle.com/projects";
        private const string errorRegistrationURL = "https://debugle.com/login";
        private const string email = "taras.globa2@gmail.com";
        private const string password = "123456Qwerty";
        [SetUp]
        public void Setup()
        {

            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://debugle.com/");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
        [Theory]
        [TestCase(email, password, "new test item2", "new test item")]
        public void CheckItemCreated(string email,string password,string item,string expected)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignInPageObject signInPageObject = new SignInPageObject(driver);
            MainMenuPageObject mainMenuPageObject = new MainMenuPageObject(driver);
            ProjectPageObject projectPageObject = new ProjectPageObject(driver);
            var signinbutt = startPageObgect.GetSignInButton();
            signinbutt.Click();
            Thread.Sleep(300);
            signInPageObject.LogIn(email, password);
            var project = mainMenuPageObject.GetGoToProjectButton();
            project.Click();
            projectPageObject.CreateItem(item);
            string actual = "";
            System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> webElements = driver.FindElements(By.Id("open-issues"));
            for (int i = 0; i < webElements.Count; i++)
            {
                if (webElements[i].Text.Contains(item))
                    actual = item;//how take just name
            }
            Assert.AreEqual(actual, expected);
        }
        [Theory]
        [TestCase(email, password, "new item3", "new item3")]
        public void CheckMarkItemAsImportant(string email, string password, string item, string expected)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignInPageObject signInPageObject = new SignInPageObject(driver);
            MainMenuPageObject mainMenuPageObject = new MainMenuPageObject(driver);
            ProjectPageObject projectPageObject = new ProjectPageObject(driver);
            var signinbutt = startPageObgect.GetSignInButton();
            signinbutt.Click();
            Thread.Sleep(300);
            signInPageObject.LogIn(email, password);
            var project = mainMenuPageObject.GetGoToProjectButton();
            project.Click();
            projectPageObject.CreateItem(item);
            projectPageObject.MarkItemAsImportant();
            var importantlist = projectPageObject.GetImportantList();
            importantlist.Click();
            driver.Navigate().Refresh();
            var actual = "";
            System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> webElements = driver.FindElements(By.Id("open-issues"));
            for (int i = 0; i < webElements.Count; i++)
            {
                if (webElements[i].Text.Contains(item))
                    actual = item;//how take just name
            }
            Assert.AreEqual(actual, expected);
        }
        [Theory]
        [TestCase(email, password, "new item3", "new item3")]
        public void CheckProjectRenamed(string email, string password, string newname, string expected)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignInPageObject signInPageObject = new SignInPageObject(driver);
            MainMenuPageObject mainMenuPageObject = new MainMenuPageObject(driver);
            ProjectPageObject projectPageObject = new ProjectPageObject(driver);
            var signinbutt = startPageObgect.GetSignInButton();
            signinbutt.Click();
            Thread.Sleep(300);
            signInPageObject.LogIn(email, password);
            var project = mainMenuPageObject.GetGoToProjectButton();
            project.Click();
            projectPageObject.ChangeProjectName(newname);
            var actual = projectPageObject.GetNewProjectName();
          
            Assert.AreEqual(actual, expected);
        }
        [Theory]
        [TestCase(email, password)]
        public void CheckProjectRemove(string email, string password)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignInPageObject signInPageObject = new SignInPageObject(driver);
            MainMenuPageObject mainMenuPageObject = new MainMenuPageObject(driver);
            ProjectPageObject projectPageObject = new ProjectPageObject(driver);
            var signinbutt = startPageObgect.GetSignInButton();
            signinbutt.Click();
            Thread.Sleep(300);
            signInPageObject.LogIn(email, password);
            var project = mainMenuPageObject.GetGoToProjectButton();
            project.Click();
            projectPageObject.DeleteProject();

            Assert.AreEqual(UrlAfterRemoveproject, projectPageObject.GetUrl()) ;
        }
    }
}
