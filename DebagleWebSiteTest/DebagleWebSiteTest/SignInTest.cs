﻿using DebagleWebSiteTest.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace DebagleWebSiteTest
{
    public class SignInTest
    {
        IWebDriver driver;
        private const string successRegistrationURL = "https://debugle.com/projects";
        private const string email = "taras.globa2@gmail.com";
        private const string password = "123456Qwerty";

        [SetUp]
        public void Setup()
        {

            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://debugle.com/");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
        [Theory]
        [TestCase(email,password, successRegistrationURL)]
        public void CheckAuthorizationWithValidData( string email, string password, string expUrl)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignInPageObject signInPageObject = new SignInPageObject(driver);
            var signupbutt = startPageObgect.GetSignUpButton();
            signupbutt.Click();
            Thread.Sleep(300);
            signInPageObject.LogIn(email, password);
            var actualUrl = signInPageObject.GetUrl();
            Assert.AreEqual(actualUrl, expUrl);
        }
        [Theory]
        [TestCase(email, password, successRegistrationURL, "The email and password don't match!")]
        public void CheckAuthorizationWithNotValidData(string email, string password, string expUrl,string experror)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignInPageObject signInPageObject = new SignInPageObject(driver);
            var signupbutt = startPageObgect.GetSignUpButton();
            signupbutt.Click();
            Thread.Sleep(300);
            signInPageObject.LogIn(email, password);
            var actualUrl = signInPageObject.GetUrl();
            var actualerror = signInPageObject.GetAuthorizationError();
            Assert.AreEqual(actualUrl, expUrl);
            Assert.AreEqual(actualerror, experror);

        }
    }
}
