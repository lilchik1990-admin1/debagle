﻿using DebagleWebSiteTest.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace DebagleWebSiteTest
{
    class PersonalSettingsTest
    {
        IWebDriver driver;
        private const string successRegistrationURL = "https://debugle.com/projects";
        private const string successAuthorizationURL = "https://debugle.com/projects";
        private const string email = "taras.globa2@gmail.com";
        private const string password = "123456Qwerty";

        [SetUp]
        public void Setup()
        {

            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://debugle.com/");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
        [Theory]
        [TestCase(email, password, "Taras","Globa", "taras.globa2@gmail.com", "Taras", "Globa", "taras.globa2@gmail.com", "(GMT-06:00) Central Time (US & Canada)")]
        public void CheckPersonalDataChanging(string email, string password, string newname, string newlastname, string newemail,string exp1, string exp2,string exp3, string exp4)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignInPageObject signInPageObject = new SignInPageObject(driver);
            MyAccauntPajeObject myAccauntPajeObject = new MyAccauntPajeObject(driver);
            var signinbutt = startPageObgect.GetSignInButton();
            signinbutt.Click();
            Thread.Sleep(300);
            signInPageObject.LogIn(email, password);

            myAccauntPajeObject.ChangePersonalData(newname, newlastname, newemail);
            var actualname = myAccauntPajeObject.GetActualFirstName();
            var actuallastname = myAccauntPajeObject.GetActualLasttName();
            var actualemail = myAccauntPajeObject.GetActualEmail();
            var actualtimezone = myAccauntPajeObject.GetActualTimeZone();
            
            Assert.AreEqual(actualname, exp1);
            Assert.AreEqual(actuallastname, exp2);
            Assert.AreEqual(actualemail, exp3);
            Assert.AreEqual(actualtimezone, exp4);
        }
        [Theory]
        [TestCase(email, password, "123456Qwerty", "The email and password don't match!")]
        public void CheckPasswordChanging(string email, string password, string newpass,string experror)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignInPageObject signInPageObject = new SignInPageObject(driver);
            MyAccauntPajeObject myAccauntPajeObject = new MyAccauntPajeObject(driver);
            var signinbutt = startPageObgect.GetSignInButton();
            signinbutt.Click();
            Thread.Sleep(300);
            signInPageObject.LogIn(email, password);

            myAccauntPajeObject.ChangePassword(password, newpass, newpass);
            myAccauntPajeObject.LogOut();
            signInPageObject.LogInWithNewPassword(email, password);
            var actualerror = signInPageObject.GetAuthorizationError();
            Assert.AreEqual(actualerror, experror);
            signInPageObject.LogInWithNewPassword(email, newpass);
            Assert.AreEqual(signInPageObject.GetUrl(), successAuthorizationURL);
           
        }
        [Theory]
        [TestCase(email, password,  "Because i want it")]
        public void CheckAccauntCancel(string email, string password, string comment)
        {
            StartPageObgect startPageObgect = new StartPageObgect(driver);
            SignInPageObject signInPageObject = new SignInPageObject(driver);
            MyAccauntPajeObject myAccauntPajeObject = new MyAccauntPajeObject(driver);
            var signinbutt = startPageObgect.GetSignInButton();
            signinbutt.Click();
            Thread.Sleep(300);
            signInPageObject.LogIn(email, password);

            myAccauntPajeObject.RemoveAccaunt(comment);
            myAccauntPajeObject.LogOut();
            signInPageObject.LogInWithNewPassword(email, password);
            var actual = myAccauntPajeObject.GetCanselMessege();
            Assert.AreEqual(actual, comment);

        }
    }
}
