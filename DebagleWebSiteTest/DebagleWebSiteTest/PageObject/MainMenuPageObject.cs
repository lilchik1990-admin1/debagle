﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebagleWebSiteTest.PageObject
{
    public class MainMenuPageObject
    {
        private IWebDriver webdriver;
        private readonly By _goToProjectButton = By.CssSelector("#projects-index > div > div > a");
        public IWebElement GetGoToProjectButton() => webdriver.FindElement(_goToProjectButton);

        public MainMenuPageObject(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }
        
        

    }
}
